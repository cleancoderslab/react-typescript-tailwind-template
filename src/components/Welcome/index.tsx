import React from 'react'

const Welcome: React.FC = () => {
  return <h1 className='text-3xl font-bold underline'>Bienvenido/a!</h1>
}

export default Welcome
