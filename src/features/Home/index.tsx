import React from 'react'

// Components
import Welcome from '../../components/Welcome'

const Home: React.FC = () => {
  return <Welcome />
}

export default Home
