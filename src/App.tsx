import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

// Views
import Home from './features/Home'

const App: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route path='/' element={Home} />
      </Routes>
    </Router>
  )
}

export default App
